﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ZTP_BAZY.Models;

namespace ZTP_BAZY.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ModelContext _context;

        public HomeController(ILogger<HomeController> logger, ModelContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated == true)
                return RedirectToAction("Logged");
            return View();
        }
        [HttpGet]
        public IActionResult Login(string ReturnUrl = "/Home/Logged")
        {
            if(User.Identity.IsAuthenticated) return Redirect(ReturnUrl);
            ViewData["ReturnUrl"] = ReturnUrl;
            return View();
        }
        [HttpGet]
        public IActionResult Logged()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("CookieAuthentication");
            
            return RedirectToAction("Login");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserCredentials userCredentials, string ReturnUrl)
        {
            ViewData["ReturnUrl"] = ReturnUrl;

            var user = _context.User.FirstOrDefault(user => user.Name == userCredentials.User.Name);
            if(user == null)
            {
                ViewData["error"] = "Podana nazwa użytkownika nie istnieje";
                return View();
            }
            else
            {
                user.Credentials = _context.Credentials.FirstOrDefault(u => u.Userid == user.Userid);
                userCredentials.User.Credentials = userCredentials.Credentials;
                user.Role = _context.Role.FirstOrDefault(role => role.Roleid == user.Roleid);
                if (ValideteUser(userCredentials.User, user.Credentials.Password))
                {
                    var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Name,userCredentials.User.Name),
                        new Claim(ClaimTypes.Role,user.Role.Rolename)

                    };
                    var claimsIdentity = new ClaimsIdentity(claims, "CookieAuthentication");

                    await HttpContext.SignInAsync("CookieAuthentication", new
                    ClaimsPrincipal(claimsIdentity), new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
                    }) ;

                    return Redirect(ReturnUrl);
                }
            }
            ViewData["error"] = "Hasło jest nieprawidłowe";
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private bool ValideteUser(User user, string password)
        {

            var hasher = new PasswordHasher<string>();
            if (hasher.VerifyHashedPassword(user.Name, password, user.Credentials.Password) == PasswordVerificationResult.Success)
            {
                return true;
            }
            else return false;
        }
    }
}
