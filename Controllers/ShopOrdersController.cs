﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZTP_BAZY.Models;

namespace ZTP_BAZY.Controllers
{
    [Authorize(Roles = "sklep")]
    public class ShopOrdersController : Controller
    {
        private readonly ModelContext _context;
        
        public ShopOrdersController(ModelContext context)
        {
            _context = context;
            
        }
        [HttpGet]
        public async Task<IActionResult> Index(string searchString, int? pageNumber, string currentFilter)
        {
            if (Request.Cookies["shop"] == null)
            {
                if (searchString != null)
                {
                    pageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                var Shops = _context.Shop;
                int pageSize = 20;
                if (!String.IsNullOrEmpty(searchString))
                {
                    var ShopList = Shops.Where(s => s.Shopname.Contains(searchString));
                    var List = await PaginatedList<Shop>.CreateAsync(ShopList, pageNumber ?? 1, pageSize);

                    return View(List);
                }
                var Listt = await PaginatedList<Shop>.CreateAsync(Shops, pageNumber ?? 1, pageSize);

                return View(Listt);
            }

            else return RedirectToAction("Order");
        }
        [HttpGet]
        public IActionResult Order(int? id, string message)
        {
            if(message != null)
            {
                ViewData["error"] = message;
            }
            int shopId;
            if (Request.Cookies["shop"] == null && id == null)
            {
                return RedirectToAction("Index");
            }
            else if (Request.Cookies["shop"] != null)
            {
                shopId = Int32.Parse(Request.Cookies["shop"]);
            }
            else
            {
                shopId = (int)id;
                Response.Cookies.Append("shop", shopId.ToString());
            }
            Shop shop = _context.Shop.FirstOrDefault(shop => shop.Shopid == shopId);
            ViewData["shopName"] = $"{shop.Shopname}\n {shop.Street} {shop.Number}\n {shop.Zipcode} {shop.City}";
            List<ProductWithCount> productWithCounts;
            if (Request.Cookies["items"] != null)
            productWithCounts = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["items"]);
            else
            productWithCounts = new List<ProductWithCount>();
            return View(productWithCounts);
        }
        [HttpPost]
        public async Task<IActionResult> Order()
        {
            List<ProductWithCount> productWithCounts;
            if (Request.Cookies["items"] != null)
            {
                productWithCounts = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["items"]);
                if(productWithCounts.Count == 0)
                {
                    string message = "Zamówienie jest puste";
                    return RedirectToAction("Order", new { message = message });
                }
                int shopId = Int32.Parse(Request.Cookies["shop"]);
                Order order = new Order()
                {
                    Shopid = shopId,
                    Statusid = 1,
                    Creationdate = DateTime.Now,
                    Priority = 1
                };
                order.Orderhistory.Add(new Orderhistory() { Statusid = 1, Changedate = DateTime.Now });
                foreach(var i in productWithCounts)
                {
                    
                    order.Productorder.Add(new Productorder() { Productid = i.Product.Productid, Count = i.Count });
                    var product = _context.Product.FirstOrDefault(p => p.Productid == i.Product.Productid);
                    product.Demand += i.Count; 
                    
                    _context.Product.Attach(product);
                    _context.Entry(product).Property(x => x.Demand).IsModified = true;

                }

                _context.Order.Add(order);
                await _context.SaveChangesAsync();
                Response.Cookies.Append("shop","", new CookieOptions()
                {
                    Expires = DateTime.Now.AddDays(-1)
                });
               Response.Cookies.Append("items", "", new CookieOptions()
                {
                    Expires = DateTime.Now.AddDays(-1)
                });
                return RedirectToAction("OrderSuccess", order);
            }
            else
            {
                string message = "Zamówienie jest puste";
                return RedirectToAction("Order", new { message = message });
            }
        }
        [HttpGet]
        public IActionResult OrderSuccess(Order order)
        {
            return View(order);
        }
        [HttpGet]
        public async Task<IActionResult> AddProduct(string searchString, int? pageNumber, string currentFilter)
        {
            if (Request.Cookies["shop"] == null)
            {
                return RedirectToAction("Index");
            }

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;
            var Products = _context.Product;
            int pageSize = 20;
            if (!String.IsNullOrEmpty(searchString))
            {
                var ProductList = Products.Where(s => s.Name.Contains(searchString));
                var List = await PaginatedList<Product>.CreateAsync(ProductList, pageNumber ?? 1, pageSize);
                List.Sort((el1, el2) => el1.Demand > el2.Demand ? -1 : 1);
                return View(List);
            }
            var Listt = await PaginatedList<Product>.CreateAsync(Products, pageNumber ?? 1, pageSize);
            Listt.Sort((el1, el2) => el1.Demand > el2.Demand ? -1 : 1);
            return View(Listt);
        }
        [HttpPost]
        public async Task<IActionResult> AddProduct(int id)
        {
            if (Request.Cookies["items"] != null)
            {
                var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["items"]);
                var Product = _context.Product.FirstOrDefault(p => p.Productid == id);
                var Product2 = items.Find(u => u.Product.Productid == Product.Productid);
                if(Product2 == null)
                {
                    items.Add(new ProductWithCount() { Count = 1, Product = Product });
                    Response.Cookies.Append("items", JsonSerializer.Serialize<List<ProductWithCount>>(items));
                }
                else
                {
                    Product2.Count++;
                    Response.Cookies.Append("items", JsonSerializer.Serialize<List<ProductWithCount>>(items));
                }
                
            }
            else
            {
                var items = new List<ProductWithCount>();
                var Product = _context.Product.FirstOrDefault(p => p.Productid == id);
                items.Add(new ProductWithCount() { Count = 1, Product = Product });
                Response.Cookies.Append("items", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            }
            return RedirectToAction("Order");
        }
        [HttpGet]
        public IActionResult Add(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Order");
            }
            var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["items"]);
            var item = items.Find(i => i.Product.Productid == (int)id);
            if(item == null)
            {
                return RedirectToAction("Order");
            }
            item.Count++;
            Response.Cookies.Append("items", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            return RedirectToAction("Order");
        }
        [HttpGet]
        public IActionResult Sub(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Order");
            }
            var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["items"]);
            var item = items.Find(i => i.Product.Productid == (int)id);
            if(item == null)
            {
                return RedirectToAction("Order");
            }
            if(item.Count >1)
            item.Count--;
            Response.Cookies.Append("items", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            return RedirectToAction("Order");
        }
        [HttpPost]
        public IActionResult Clear()
        {
            Response.Cookies.Append("shop", "", new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(-1)
            });
            Response.Cookies.Append("items", "", new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(-1)
            });
            return RedirectToAction("Order");
        }
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Order");
            }
            var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["items"]);
            int index = items.FindIndex(i => i.Product.Productid == (int)id);
            items.RemoveAt(index);
           
            Response.Cookies.Append("items", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            return RedirectToAction("Order");
        }
    }
    }

