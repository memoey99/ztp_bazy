using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZTP_BAZY.Models;

namespace ZTP_BAZY.Controllers
{
    [Authorize(Roles = "sklep")]
    public class OrdersController : Controller
    {
        private readonly ModelContext _context;

        public OrdersController(ModelContext context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<IActionResult> Index(string searchString, int? pageNumber, string currentFilter)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;
            var Shops = _context.Shop;
            int pageSize = 20;
            if (!String.IsNullOrEmpty(searchString))
            {
                var ShopList = Shops.Where(s => s.Shopname.Contains(searchString));
                var List = await PaginatedList<Shop>.CreateAsync(ShopList, pageNumber ?? 1, pageSize);

                return View(List);
            }
            var Listt = await PaginatedList<Shop>.CreateAsync(Shops, pageNumber ?? 1, pageSize);

            return View(Listt);
        }
        [HttpGet]
        public async Task<IActionResult> List(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            int shopId = (int)id;
            Shop shop = _context.Shop.FirstOrDefault(shop => shop.Shopid == shopId);

            ViewData["shopName"] = $"{shop.Shopname}, {shop.Street} {shop.Number}, {shop.Zipcode} {shop.City}";
            var orders = await _context.Order.Include(o => o.Orderhistory).Where(s => s.Shopid == id).ToListAsync();
            orders.Sort((x, y) => x.Creationdate > y.Creationdate ? -1 : 1);
            Dictionary<int, string> statuses = await _context.Status.ToDictionaryAsync(s => s.Statusid, s => s.Name);
/*             var dbStatuses = await _context.Status.ToListAsync();
            foreach (var s in dbStatuses)
            {
                statuses.Add(s.Statusid, s.Name);
            } */
            ViewBag.statuses = statuses;

            return View(orders);
        }
        //TODO: szczegoly zamowienia: order+poroductorder + orderhistory
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
                //return RedirectToAction("List", new { id = shopId }); //TODO: naprawić żeby przekierowało na listę z podanym id
            }
            int orderId = (int)id;
            OrderDetails details = new OrderDetails();
            details.Order = _context.Order.FirstOrDefault(order => order.Orderid == orderId);
            //var productOrders = _context.Productorder.Where(x => x.Orderid == orderId)?.Select(column => column.Productid);
            //details.Products = await _context.Product.Where(p => productOrders.Contains(p.Productid)).ToListAsync();
            details.Products = await _context.Productorder.Where(x => x.Orderid == orderId).ToListAsync();
            details.history = await _context.Orderhistory.Include(o => o.Status).Where(h => h.Orderid == orderId).ToListAsync();
            Dictionary<int, string> statuses = await _context.Status.ToDictionaryAsync(s => s.Statusid, v => v.Name);
            ViewBag.statuses = statuses;
            Dictionary<int, string> productNames = await _context.Product.ToDictionaryAsync(p => p.Productid, v => v.Name);
            ViewBag.names = productNames; 
            return View(details);
        }
    }
}