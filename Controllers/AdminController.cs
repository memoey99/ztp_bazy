﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ZTP_BAZY.Models;

namespace ZTP_BAZY.Controllers

{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {

        private readonly ModelContext _context;
        private PasswordHasher<string> hasher;
        public AdminController(ModelContext context)
        {
            _context = context;
            hasher = new PasswordHasher<string>();
        }
        
        public IActionResult Index()
        {
            return RedirectToAction("UserList");
        }

        public async Task<IActionResult> UserList(string searchString, int? pageNumber, string currentFilter)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;
            
            var userList = _context.User.Include(u => u.Role);
            int pageSize = 20;
            if(!String.IsNullOrEmpty(searchString))
            {
                var users = userList.Where(s => s.Name.Contains(searchString));
                return View(await PaginatedList<User>.CreateAsync(users,pageNumber ?? 1,pageSize));
            }
            return View(await PaginatedList<User>.CreateAsync(userList, pageNumber ?? 1, pageSize));
        }
        [HttpGet]
        public IActionResult CreateUser()
        {
            ViewData["Roleid"] = new SelectList(_context.Role, "Roleid", "Rolename");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(UserCredentials userCredentials)
        {
            if (ModelState.IsValid)
            {
                if (_context.User.FirstOrDefault(u => u.Name == userCredentials.User.Name) == null)
                {
                    
                    userCredentials.Credentials.Password = hasher.HashPassword(userCredentials.User.Name, userCredentials.Credentials.Password);
                    userCredentials.User.Credentials = userCredentials.Credentials;
                    userCredentials.User.Creationdate = DateTime.Now;
                    _context.Add(userCredentials.User);
                    _context.Add(userCredentials.Credentials);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["Error"] = "Nazwa jest zajęta";
            ViewData["Roleid"] = new SelectList(_context.Role, "Roleid", "Rolename", userCredentials.User.Roleid);
            return View(userCredentials);
        }
        [HttpGet]
        public async Task<IActionResult> EditUser(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            ViewData["Roleid"] = new SelectList(_context.Role, "Roleid", "Rolename", user.Roleid);
            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUser(User user,int id)
        {
            if (id != user.Userid)
            {
                return NotFound();
            }
            _context.User.Attach(user);
            _context.Entry(user).Property(x => x.Roleid).IsModified = true;
            await _context.SaveChangesAsync();
            return RedirectToAction("UserList");
        }
        [HttpGet]
        public async Task<IActionResult> UserDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .Include(u => u.Role)
                .FirstOrDefaultAsync(m => m.Userid == id);
            
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }
        [HttpGet]
        public async Task<IActionResult> DeleteUser(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .Include(u => u.Role)
                .FirstOrDefaultAsync(m => m.Userid == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }
        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _context.User.FindAsync(id);
            //var credentials = await _context.Credentials.FindAsync(id);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public async Task<IActionResult> EditPassword(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.FindAsync(id);
            var credentials = await _context.Credentials.FindAsync(id);
            if (user == null || credentials == null)
            {
                return NotFound();
            }

            return View(new UserCredentials() { User = user, Credentials = credentials });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPassword(UserCredentials userCredentials, int id)
        {
            if (id != userCredentials.User.Userid)
            {
                return NotFound();
            }
          userCredentials.Credentials.Password = hasher.HashPassword(userCredentials.User.Name, userCredentials.Credentials.Password);
            userCredentials.Credentials.Userid = id;
            _context.Credentials.Attach(userCredentials.Credentials);
            _context.Entry(userCredentials.Credentials).Property(x => x.Password).IsModified = true;
            await _context.SaveChangesAsync();
            return RedirectToAction("UserList");
        }
        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.Userid == id);
        }
    }
}
