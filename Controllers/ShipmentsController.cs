using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZTP_BAZY.Models;

namespace ZTP_BAZY.Controllers
{
    [Authorize(Roles = "Administrator, pracownik magazynu")]
    public class ShipmentsController : Controller
    {

        private readonly ModelContext _context;

        public ShipmentsController(ModelContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<IActionResult> Index(string searchString, int? pageNumber, string currentFilter)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;
            var OrdersWithShop = _context.Order
            .Join(_context.Status, order => order.Statusid, status => status.Statusid, (order, status) => new
            {
                Data = order,
                Status = status.Name
            })
            .Join(_context.Shop, order => order.Data.Shopid, shop => shop.Shopid, (order, shop) => new
            {
                ShopName = shop.Shopname,
                ShopAdress = (shop.Street + " " + shop.Number + ", " + shop.Zipcode + " " + shop.City),
                ShopPhoneNumber = shop.Phonenumber,
                OrderId = order.Data.Orderid,
                StatusId = order.Data.Statusid,
                Status = order.Status,
                Description = order.Data.Description,
                CreationDate = order.Data.Creationdate
                //Priority = order.Priority
            });
            var orderList = OrdersWithShop.Select(ob => new OrderWithShop()
            {
                ShopName = ob.ShopName,
                ShopAdress = ob.ShopAdress,
                Orderid = ob.OrderId,
                Status = ob.Status,
                Description = ob.Description,
                Creationdate = ob.CreationDate,
                Statusid = ob.StatusId
            }).OrderBy(s => s.Statusid); //TODO: poprawić sort

            int pageSize = 20;
            if (!String.IsNullOrEmpty(searchString))
            {
                var orderListSearched = orderList.Where(s => s.ShopName.Contains(searchString));
                var List = await PaginatedList<OrderWithShop>.CreateAsync(orderListSearched, pageNumber ?? 1, pageSize);
                List.Sort((el1,el2) => {
                    if (el1.Statusid == el2.Statusid)
                    {
                        if (el1.Creationdate > el2.Creationdate) return 1;
                        else return -1;
                    }
                    else if (el1.Statusid > el2.Statusid) return 1;
                    return -1;
                });
                return View(List);
            }
            var Listt = await PaginatedList<OrderWithShop>.CreateAsync(orderList, pageNumber ?? 1, pageSize);
            Listt.Sort((el1, el2) => {
                if (el1.Statusid == el2.Statusid)
                {
                    if (el1.Creationdate > el2.Creationdate) return 1;
                    else return -1;
                }
                else if (el1.Statusid > el2.Statusid) return 1;
                return -1;
            });
            return View(Listt);
        }
       
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            int orderId = (int)id;
            var detailsFromDB = _context.Order.Where(order => order.Orderid == orderId)
                .Include(o => o.Orderhistory)
                .Join(_context.Productorder, o => o.Orderid, po => po.Orderid, (order, productOrder) => new
                {
                    Order = order,
                    Count = productOrder.Count,
                    ProductOrder = productOrder
                }).Join(_context.Product, po => po.ProductOrder.Productid, p => p.Productid, (po, p) => new
                {
                    Order = po.Order,
                    Count = po.Count,
                    ProductOrder = po.ProductOrder,
                    Product = p
                });

            OrderDetails details = new OrderDetails();
            details.Order = detailsFromDB.First().Order;
            details.Products = detailsFromDB.Select(p => p.ProductOrder).ToList();
            details.ProductsList = detailsFromDB.Select(p => p.Product).ToList();
            details.history = detailsFromDB.First().Order.Orderhistory.ToList();

            Dictionary<int, string> statuses = await _context.Status.ToDictionaryAsync(s => s.Statusid, v => v.Name);
            ViewBag.statuses = statuses;

            return View(details);
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int? id, string message)
        {
            if (id == null) return RedirectToAction("Index");
            if (message != null)
            {
                ViewData["error"] = message;
            }
            int orderId = (int)id;
            Order order = await _context.Order.FindAsync(id);

            if (order == null) return RedirectToAction("Index");

            List<Status> statuses = await _context.Status.Where(s => order.Statusid < s.Statusid).ToListAsync();
            ViewBag.statuses = statuses;

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Orderid,Name,Shopid,Statusid,Description,Creationdate,Priority")] Order order)
        {
            if (id != order.Orderid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //TODO: zmienić to jakoś na enum czy cokolwiek łatwiejszego do zmiany -> status wysłane
                    if (order.Statusid == 3)
                    {
                        var products = _context.Productorder.Where(p => p.Order.Orderid == order.Orderid)
                        .Join(_context.Product, po => po.Productid, p => p.Productid, (po, p) => new
                        {
                            ProductOrder = po,
                            Product = p
                        });
                        var tx = _context.Database.BeginTransaction();
                        foreach (var p in products)
                        {
                            if (p.ProductOrder.Count > p.Product.Count)
                            {
                                tx.Rollback();
                                string message = "Zamówienie nie może być zrealizowane, za mało produktów na stanie";
                                return RedirectToAction("Edit", new { id = id, order = order, message = message });
                                //return RedirectToAction("Index"); //TODO: return error
                            }
                            Product product = p.Product;
                            product.Demand -= p.ProductOrder.Count;
                            product.Count -= p.ProductOrder.Count;
                            _context.Update(product);
                        }
                        tx.Commit();
                    }
                    if (order.Statusid != 0)
                    {
                        _context.Update(order);
                        Orderhistory orderhistory = new Orderhistory()
                        {
                            Orderid = order.Orderid,
                            Statusid = (int)order.Statusid,
                            Changedate = DateTime.Now
                        };
                        _context.Add(orderhistory);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        _context.Order.Attach(order);
                        _context.Entry(order).Property(o => o.Description).IsModified = true;
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Orderid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }
        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.Orderid == id);
        }
    }
}