﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZTP_BAZY.Models;

namespace ZTP_BAZY.Controllers
{
    [Authorize(Roles = "Administrator,pracownik magazynu")]
    public class DeliveriesController : Controller
    {
        private readonly ModelContext _context;
        public DeliveriesController(ModelContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(string searchString, int? pageNumber, string currentFilter)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;
            var Deliveries = _context.Delivery.Include(d => d.Supplier);
            int pageSize = 20;
            if (!String.IsNullOrEmpty(searchString))
            {
                int id = Int32.Parse(searchString);
                var DeliveryList = Deliveries.Where(s => s.Deliveryid == id);

                var List = await PaginatedList<Delivery>.CreateAsync(DeliveryList, pageNumber ?? 1, pageSize);
                List.Sort((el1, el2) => el1.Deliverydate > el2.Deliverydate ? -1 : 1);
                return View(List);
            }
            var Listt = await PaginatedList<Delivery>.CreateAsync(Deliveries, pageNumber ?? 1, pageSize);
            Listt.Sort((el1, el2) => el1.Deliverydate > el2.Deliverydate ? -1 : 1);
            return View(Listt);
            
        }
        [HttpGet]
        public async Task<IActionResult> NewDelivery(string searchString, int? pageNumber, string currentFilter)
        {
            if (Request.Cookies["supplier"] == null)
            {
                if (searchString != null)
                {
                    pageNumber = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                var Suppliers = _context.Supplier;
                int pageSize = 20;
                if (!String.IsNullOrEmpty(searchString))
                {
                    var SupplierList = Suppliers.Where(s => s.Name.Contains(searchString));
                    var List = await PaginatedList<Supplier>.CreateAsync(SupplierList, pageNumber ?? 1, pageSize);

                    return View(List);
                }
                var Listt = await PaginatedList<Supplier>.CreateAsync(Suppliers, pageNumber ?? 1, pageSize);

                return View(Listt);
            }

            else return RedirectToAction("NewDeliveryProducts");
        }
        [HttpGet]
        public IActionResult NewDeliveryProducts(int? id, string message)
        {
            if(message != null)
            {
                ViewData["error"] = message;
            }
            int SupplierID;
            if (Request.Cookies["supplier"] == null && id == null)
            {
                return RedirectToAction("Index");
            }
            else if (Request.Cookies["supplier"] != null)
            {
                SupplierID = Int32.Parse(Request.Cookies["supplier"]);
            }
            else
            {
                SupplierID = (int)id;
                Response.Cookies.Append("supplier", SupplierID.ToString());
            }
            Supplier supplier = _context.Supplier.FirstOrDefault(s => s.Supplierid == SupplierID);
            ViewData["supplierName"] = $"{supplier.Name}<br/> {supplier.Street} {supplier.Number}<br/> {supplier.Zipcode} {supplier.City}<br/> strona: {supplier.Website}";
            List<ProductWithCount> productWithCounts;
            if (Request.Cookies["DeliveryItems"] != null)
                productWithCounts = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["DeliveryItems"]);
            else
                productWithCounts = new List<ProductWithCount>();
            return View(productWithCounts);
        }
        [HttpPost]
        public async Task <IActionResult> NewDeliveryProducts()
        {
            List<ProductWithCount> productWithCounts;
            if(Request.Cookies["DeliveryItems"] != null)
            {
                productWithCounts = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["DeliveryItems"]);
                if (productWithCounts.Count == 0)
                {

                    string message = "Brak wprowadzonych produktów";
                    return RedirectToAction("NewDeliveryProducts",new { message = message });
                }
                int SupplierId = Int32.Parse(Request.Cookies["supplier"]);
                Delivery delivery = new Delivery()
                {
                    Deliverydate = DateTime.Now,
                    Supplierid = SupplierId,

                };
                foreach(ProductWithCount p in productWithCounts)
                {
                    delivery.Productdelivery.Add(new Productdelivery()
                    {
                        Productid = p.Product.Productid,
                        Count = p.Count
                    });
                    Product product = await _context.Product.FirstOrDefaultAsync(pr => pr.Productid == p.Product.Productid);
                    product.Count += p.Count;
                    product.Lastdeliverydate = delivery.Deliverydate;
                    _context.Product.Attach(product);
                    
                    _context.Entry(product).Property(x => x.Count).IsModified = true;
                    _context.Entry(product).Property(x => x.Lastdeliverydate).IsModified = true;
                }
                _context.Delivery.Add(delivery);
                await _context.SaveChangesAsync();
                Response.Cookies.Append("supplier", "", new CookieOptions()
                {
                    Expires = DateTime.Now.AddDays(-1)
                });
                Response.Cookies.Append("DeliveryItems", "", new CookieOptions()
                {
                    Expires = DateTime.Now.AddDays(-1)
                });
                return RedirectToAction("Success");
            }
            else
            {
                string message = "Brak wprowadzonych produktów";
                return RedirectToAction("NewDeliveryProducts", new { message = message });
            }
        }
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return RedirectToAction("Index");
            var ProductsDeliveries = await _context.Productdelivery.Include(pd => pd.Delivery).Include(pd => pd.Product).Where(pd => pd.Deliveryid == id).ToListAsync();
            ProductsDeliveries[0].Delivery.Supplier = await _context.Supplier.FirstOrDefaultAsync(o => o.Supplierid == ProductsDeliveries[0].Delivery.Supplierid);
            return View(ProductsDeliveries);
        }

        [HttpGet]
        public IActionResult Success()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> AddProduct(string searchString, int? pageNumber, string currentFilter)
        {
            if (Request.Cookies["supplier"] == null)
            {
                return RedirectToAction("NewDelivery");
            }

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;
            var Products = _context.Product;
            int pageSize = 20;
            if (!String.IsNullOrEmpty(searchString))
            {
                var ProductList = Products.Where(s => s.Name.Contains(searchString));
                var List = await PaginatedList<Product>.CreateAsync(ProductList, pageNumber ?? 1, pageSize);
                List.Sort((el1, el2) => el1.Demand > el2.Demand ? -1 : 1);
                return View(List);
            }
            var Listt = await PaginatedList<Product>.CreateAsync(Products, pageNumber ?? 1, pageSize);
            Listt.Sort((el1, el2) => el1.Demand > el2.Demand ? -1 : 1);
            return View(Listt);
        }
        [HttpPost]
        public  IActionResult AddProduct(int id)
        {
            if (Request.Cookies["DeliveryItems"] != null)
            {
                var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["DeliveryItems"]);
                var Product = _context.Product.FirstOrDefault(p => p.Productid == id);
                var Product2 = items.Find(u => u.Product.Productid == Product.Productid);
                if (Product2 == null)
                {
                    items.Add(new ProductWithCount() { Count = 1, Product = Product });
                    Response.Cookies.Append("DeliveryItems", JsonSerializer.Serialize<List<ProductWithCount>>(items));
                }
                else
                {
                    Product2.Count++;
                    Response.Cookies.Append("DeliveryItems", JsonSerializer.Serialize<List<ProductWithCount>>(items));
                }

            }
            else
            {
                var items = new List<ProductWithCount>();
                var Product = _context.Product.FirstOrDefault(p => p.Productid == id);
                items.Add(new ProductWithCount() { Count = 1, Product = Product });
                Response.Cookies.Append("DeliveryItems", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            }
            return RedirectToAction("NewDeliveryProducts");
        }
        [HttpGet]
        public IActionResult Add(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NewDeliveryProducts");
            }
            var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["DeliveryItems"]);
            var item = items.Find(i => i.Product.Productid == (int)id);
            if (item == null)
            {
                return RedirectToAction("NewDeliveryProducts");
            }
            item.Count++;
            Response.Cookies.Append("DeliveryItems", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            return RedirectToAction("NewDeliveryProducts");
        }
        [HttpGet]
        public IActionResult Sub(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NewDeliveryProducts");
            }
            var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["DeliveryItems"]);
            var item = items.Find(i => i.Product.Productid == (int)id);
            if (item == null)
            {
                return RedirectToAction("NewDeliveryProducts");
            }
            if (item.Count > 1)
                item.Count--;
            Response.Cookies.Append("DeliveryItems", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            return RedirectToAction("NewDeliveryProducts");
        }
        [HttpPost]
        public IActionResult Clear()
        {
            Response.Cookies.Append("supplier", "", new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(-1)
            });
            Response.Cookies.Append("DeliveryItems", "", new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(-1)
            });
            return RedirectToAction("Index");
        }
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("NewDeliveryProducts");
            }
            var items = JsonSerializer.Deserialize<List<ProductWithCount>>(Request.Cookies["DeliveryItems"]);
            int index = items.FindIndex(i => i.Product.Productid == (int)id);
            items.RemoveAt(index);

            Response.Cookies.Append("DeliveryItems", JsonSerializer.Serialize<List<ProductWithCount>>(items));
            return RedirectToAction("NewDeliveryProducts");
        }

    }
}
