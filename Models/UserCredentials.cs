﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZTP_BAZY.Models
{
    public class UserCredentials
    {
        public User User { get; set; }
        public Credentials Credentials { get; set; }
    }
}
