﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Credentials
    {
        public int Userid { get; set; }

        [DataType(DataType.Password)]
        [MaxLength(25)]
        [MinLength(8,ErrorMessage ="Hasło musi mieć co najmniej 8 znaków")]
        [Required(ErrorMessage ="Hasło jest wymagane")]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        public virtual User User { get; set; }
    }
}
