using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public class OrderDetails
    {
        public Order Order { get; set; }
        public List<Productorder> Products { get; set; }
        public List<Product> ProductsList { get; set; }
        public List<Orderhistory> history { get; set; }
    }
}