﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            Delivery = new HashSet<Delivery>(); 
        }

        [Display(Name = "ID Dostawcy")]
        public int Supplierid { get; set; }

        [Display(Name = "Nazwa")]
        [MaxLength(40, ErrorMessage = "Nazwa nie może być dłuższa niż 40 znaków")]
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        public string Name { get; set; }

        [Display(Name = "Ulica")]
        [MaxLength(40, ErrorMessage = "Ulica nie może być dłuższa niż 40 znaków")]
        [Required(ErrorMessage = "ulica jest wymagana")]
        public string Street { get; set; }

        [Display(Name = "Numer budynku")]
        [Required(ErrorMessage = "Numer budynku jest wymagany")]
        public int Number { get; set; }

        [Display(Name = "Kod pocztowy")]
        [MaxLength(6, ErrorMessage = "Kod pocztowy dostawcy nie może być dłuższy niż 6 znaków")]
        public string Zipcode { get; set; }

        [Display(Name = "Miasto")]
        [MaxLength(30, ErrorMessage = "Adres nie może być dłuższy niż 30 znaków")]
        [Required(ErrorMessage = "Lokalizacja dostawcy jest wymagana")]
        public string City { get; set; }

        [Display(Name = "Nr telefonu")]
        [MaxLength(12, ErrorMessage = "numer telefonu za długi")]
        public string Phonenumber { get; set; }

        [Display(Name = "Adres e-mail")]
        [MaxLength(30, ErrorMessage = "Adres e-mail dostawcy nie może być dłuższy niż 30 znaków")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Niepoprawny adres")]     
        public string Mailaddress { get; set; }

        [Display(Name = "Strona internetowa")]
        [MaxLength(30, ErrorMessage = "Adres internetowy nie może być dłuższa niż 30 znaków")]
        [Required(ErrorMessage = "Strona internetowa jest wymagana")]
        [RegularExpression(@"^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$", ErrorMessage = "Niepoprawny adres")]
        public string Website { get; set; }

        public virtual ICollection<Delivery> Delivery { get; set; }
    }
}
