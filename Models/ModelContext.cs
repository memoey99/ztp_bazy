﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ZTP_BAZY.Models
{
    public partial class ModelContext : DbContext
    {
        public ModelContext()
        {
        }

        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Credentials> Credentials { get; set; }
        public virtual DbSet<Delivery> Delivery { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<Orderhistory> Orderhistory { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Productdelivery> Productdelivery { get; set; }
        public virtual DbSet<Productorder> Productorder { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Rolepermission> Rolepermission { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseOracle("Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=212.33.90.213)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=xe)));Persist Security Info=True;User Id=SBD_ST_PS4_2;Password=5k4ApbxuuJeRNQTm;",
                    o => o.UseOracleSQLCompatibility("11"));
                
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "SBD_ST_PS4_2");

            modelBuilder.Entity<Credentials>(entity =>
            {
                entity.HasKey(e => e.Userid)
                    .HasName("CREDENTIALS_PK");

                entity.ToTable("CREDENTIALS");

                entity.Property(e => e.Userid)
                    .HasColumnName("USERID")
                    .HasColumnType("NUMBER(7)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(172)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Credentials)
                    .HasForeignKey<Credentials>(d => d.Userid)
                    .HasConstraintName("CREDENTIALS_USER_FK");
            });

            modelBuilder.Entity<Delivery>(entity =>
            {
                entity.ToTable("DELIVERY");

                entity.Property(e => e.Deliveryid)
                    .HasColumnName("DELIVERYID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Deliverydate)
                    .HasColumnName("DELIVERYDATE")
                    .HasColumnType("DATE")
                    .HasDefaultValueSql("SYSDATE ");

                entity.Property(e => e.Supplierid)
                    .HasColumnName("SUPPLIERID")
                    .HasColumnType("NUMBER(7)");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Delivery)
                    .HasForeignKey(d => d.Supplierid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DELIVERY_SUPPLIER_FK");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.Orderid)
                    .HasColumnName("ORDERID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("CREATIONDATE")
                    .HasColumnType("DATE")
                    .HasDefaultValueSql("SYSDATE ");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Priority)
                    .HasColumnName("PRIORITY")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Shopid)
                    .HasColumnName("SHOPID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Statusid)
                    .HasColumnName("STATUSID")
                    .HasColumnType("NUMBER(7)")
                    .HasDefaultValueSql("1");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.Shopid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ORDER_SHOP_FK");
            });

            modelBuilder.Entity<Orderhistory>(entity =>
            {
                entity.HasKey(e => new { e.Orderid, e.Statusid })
                    .HasName("ORDERHISTORY_PK");

                entity.ToTable("ORDERHISTORY");

                entity.Property(e => e.Orderid)
                    .HasColumnName("ORDERID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Statusid)
                    .HasColumnName("STATUSID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Changedate)
                    .HasColumnName("CHANGEDATE")
                    .HasColumnType("DATE");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Orderhistory)
                    .HasForeignKey(d => d.Orderid)
                    .HasConstraintName("ORDERHISTORY_ORDER_FK");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Orderhistory)
                    .HasForeignKey(d => d.Statusid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ORDERHISTORY_STATUS_FK");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.ToTable("PERMISSION");

                entity.Property(e => e.Permissionid)
                    .HasColumnName("PERMISSIONID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("PRODUCT");

                entity.Property(e => e.Productid)
                    .HasColumnName("PRODUCTID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Count)
                    .HasColumnName("COUNT")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Demand)
                    .HasColumnName("DEMAND")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Lastdeliverydate)
                    .HasColumnName("LASTDELIVERYDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Productdelivery>(entity =>
            {
                entity.HasKey(e => new { e.Deliveryid, e.Productid })
                    .HasName("PRODUCTDELIVERY_PK");

                entity.ToTable("PRODUCTDELIVERY");

                entity.Property(e => e.Deliveryid)
                    .HasColumnName("DELIVERYID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Productid)
                    .HasColumnName("PRODUCTID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Count)
                    .HasColumnName("COUNT")
                    .HasColumnType("NUMBER(7)");

                entity.HasOne(d => d.Delivery)
                    .WithMany(p => p.Productdelivery)
                    .HasForeignKey(d => d.Deliveryid)
                    .HasConstraintName("PRODUCTDELIVERY_DELIVERY_FK");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Productdelivery)
                    .HasForeignKey(d => d.Productid)
                    .HasConstraintName("PRODUCTDELIVERY_PRODUCT_FK");
            });

            modelBuilder.Entity<Productorder>(entity =>
            {
                entity.HasKey(e => new { e.Orderid, e.Productid })
                    .HasName("PRODUCTORDER_PK");

                entity.ToTable("PRODUCTORDER");

                entity.Property(e => e.Orderid)
                    .HasColumnName("ORDERID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Productid)
                    .HasColumnName("PRODUCTID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Count)
                    .HasColumnName("COUNT")
                    .HasColumnType("NUMBER(7)");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Productorder)
                    .HasForeignKey(d => d.Orderid)
                    .HasConstraintName("PRODUCTORDER_ORDER_FK");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Productorder)
                    .HasForeignKey(d => d.Productid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("PRODUCTORDER_PRODUCT_FK");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("ROLE");

                entity.Property(e => e.Roleid)
                    .HasColumnName("ROLEID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Rolename)
                    .IsRequired()
                    .HasColumnName("ROLENAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rolepermission>(entity =>
            {
                entity.HasKey(e => new { e.Roleid, e.Permissionid })
                    .HasName("ROLEPERMISSION_PK");

                entity.ToTable("ROLEPERMISSION");

                entity.Property(e => e.Roleid)
                    .HasColumnName("ROLEID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Permissionid)
                    .HasColumnName("PERMISSIONID")
                    .HasColumnType("NUMBER(7)");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.Rolepermission)
                    .HasForeignKey(d => d.Permissionid)
                    .HasConstraintName("ROLEPERMISSION_PERMISSION_FK");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Rolepermission)
                    .HasForeignKey(d => d.Roleid)
                    .HasConstraintName("ROLEPERMISSION_ROLE_FK");
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.ToTable("SHOP");

                entity.Property(e => e.Shopid)
                    .HasColumnName("SHOPID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("CITY")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Mailaddress)
                    .HasColumnName("MAILADDRESS")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Number).HasColumnType("NUMBER(7)");

                entity.Property(e => e.Phonenumber)
                    .HasColumnName("PHONENUMBER")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Shopname)
                    .IsRequired()
                    .HasColumnName("SHOPNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasColumnName("STREET")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("STATUS");

                entity.Property(e => e.Statusid)
                    .HasColumnName("STATUSID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("SUPPLIER");

                entity.Property(e => e.Supplierid)
                    .HasColumnName("SUPPLIERID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("CITY")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Mailaddress)
                    .HasColumnName("MAILADDRESS")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Number).HasColumnType("NUMBER(7)");

                entity.Property(e => e.Phonenumber)
                    .HasColumnName("PHONENUMBER")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasColumnName("STREET")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Website)
                    .IsRequired()
                    .HasColumnName("WEBSITE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("USER__UN")
                    .IsUnique();

                entity.Property(e => e.Userid)
                    .HasColumnName("USERID")
                    .HasColumnType("NUMBER(7)");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("CREATIONDATE")
                    .HasColumnType("DATE")
                    .HasDefaultValueSql("SYSDATE ");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Roleid)
                    .HasColumnName("ROLEID")
                    .HasColumnType("NUMBER(7)");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.Roleid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("USER_ROLE_FK");
            });

            modelBuilder.HasSequence("DELIVERY_DELIVERYID_SEQ");

            modelBuilder.HasSequence("ORDER_ORDERID_SEQ");

            modelBuilder.HasSequence("PERMISSION_PERMISSIONID_SEQ");

            modelBuilder.HasSequence("PRODUCT_PRODUCTID_SEQ");

            modelBuilder.HasSequence("ROLE_ROLEID_SEQ");

            modelBuilder.HasSequence("SHOP_SHOPID_SEQ");

            modelBuilder.HasSequence("STATUS_STATUSID_SEQ");

            modelBuilder.HasSequence("SUPPLIER_SUPPLIERID_SEQ");

            modelBuilder.HasSequence("USER_USERID_SEQ");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
