﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Product
    {
        public Product()
        {
            Productdelivery = new HashSet<Productdelivery>();
            Productorder = new HashSet<Productorder>();
        }
        [Display(Name ="ID Produktu")]
        public int Productid { get; set; }

        [Display(Name ="Nazwa produktu")]
        [MaxLength(30,ErrorMessage ="Nazwa nie może być dłuższa niż 30 znaków")]
        [Required(ErrorMessage ="Nazwa jest wymagana")]
        public string Name { get; set; }
        [Display(Name = "Na stanie")]
        
        public int Count { get; set; }
        [Display(Name= "Zapotrzebowanie")]
        public int? Demand { get; set; }
        [Display(Name ="Data ostatniej dostawy")]
        [DataType(DataType.DateTime)]
        public DateTime? Lastdeliverydate { get; set; }

        public virtual ICollection<Productdelivery> Productdelivery { get; set; }
        public virtual ICollection<Productorder> Productorder { get; set; }
    }
}
