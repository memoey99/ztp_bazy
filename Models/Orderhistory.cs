﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ZTP_BAZY.Models
{
    public partial class Orderhistory
    {
        public int Orderid { get; set; }
        public int Statusid { get; set; }
        [Display(Name ="Data")]
        public DateTime Changedate { get; set; }

        public virtual Order Order { get; set; }
        public virtual Status Status { get; set; }
    }
}
