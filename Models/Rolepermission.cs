﻿using System;
using System.Collections.Generic;

namespace ZTP_BAZY.Models
{
    public partial class Rolepermission
    {
        public int Roleid { get; set; }
        public int Permissionid { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual Role Role { get; set; }
    }
}
