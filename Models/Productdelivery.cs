﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Productdelivery
    {
        public int Productid { get; set; }
        public int Deliveryid { get; set; }
        [Display(Name ="Ilość")]
        public int Count { get; set; }

        public virtual Delivery Delivery { get; set; }
        public virtual Product Product { get; set; }
    }
}
