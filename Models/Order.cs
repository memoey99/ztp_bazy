﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Order
    {
        public Order()
        {
            Orderhistory = new HashSet<Orderhistory>();
            Productorder = new HashSet<Productorder>();
        }
        [Display(Name ="ID zamówienia")]
        public int Orderid { get; set; }
        public int Shopid { get; set; }
        [Display(Name ="Status")]
        public int? Statusid { get; set; }
        [Display(Name ="Opis")]
        [MaxLength(100,ErrorMessage ="Opis moze mieć maksymalnie 100 znaków")]
        public string Description { get; set; }

        [Display(Name ="Data złożenia")]
        public DateTime Creationdate { get; set; }
        public int? Priority { get; set; }

        public virtual Shop Shop { get; set; }
        public virtual ICollection<Orderhistory> Orderhistory { get; set; }
        public virtual ICollection<Productorder> Productorder { get; set; }
    }
}
