﻿using System;
using System.Collections.Generic;

namespace ZTP_BAZY.Models
{
    public partial class Permission
    {
        public Permission()
        {
            Rolepermission = new HashSet<Rolepermission>();
        }

        public int Permissionid { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Rolepermission> Rolepermission { get; set; }
    }
}
