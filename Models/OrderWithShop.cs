using System.ComponentModel.DataAnnotations;
namespace ZTP_BAZY.Models
{
    public class OrderWithShop : Order
    {
        [Display(Name = "Nazwa sklepu")]
        public string ShopName { get; set; }
        [Display(Name = "Adres sklepu")]
        public string ShopAdress { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}