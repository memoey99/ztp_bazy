﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Status
    {
        public Status()
        {
            Orderhistory = new HashSet<Orderhistory>();
        }
        public int Statusid { get; set; }
        public string Name { get; set; }

        [Display(Name="Opis")]
        public string Description { get; set; }

        public virtual ICollection<Orderhistory> Orderhistory { get; set; }
    }
}
