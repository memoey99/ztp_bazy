﻿using System;
using System.Collections.Generic;

namespace ZTP_BAZY.Models
{
    public partial class Role
    {
        public Role()
        {
            Rolepermission = new HashSet<Rolepermission>();
            User = new HashSet<User>();
        }

        public int Roleid { get; set; }
        public string Rolename { get; set; }

        public virtual ICollection<Rolepermission> Rolepermission { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
