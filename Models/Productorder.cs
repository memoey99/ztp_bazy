﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace ZTP_BAZY.Models
{
    public partial class Productorder
    {
        public int Orderid { get; set; }
        [Display(Name ="ID Produktu")]
        public int Productid { get; set; }
        [Display(Name ="Ilość")]
        public int Count { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}
