﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ZTP_BAZY.Models
{
    public class ProductWithCount
    {
        public Product Product { get; set; }
        [Display(Name ="Ilość")]
        public int Count { get; set; }
    }
}
