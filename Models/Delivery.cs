﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Delivery
    {
        public Delivery()
        {
            Productdelivery = new HashSet<Productdelivery>();
        }
        [Display(Name ="Id dostawy")]
        public int Deliveryid { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "Data dostawy")]
        public DateTime Deliverydate { get; set; }
        public int Supplierid { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<Productdelivery> Productdelivery { get; set; }
    }
}
