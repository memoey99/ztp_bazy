﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class User
    {
        public int Userid { get; set; }
        [DataType(DataType.Text)]
        [MaxLength(20, ErrorMessage = "Nazwa nie może być dłuższa niż 20 znaków")]
        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana")]
        [Display(Name = "Nazwa użytkownika")]
        
        public string Name { get; set; }
        
        [Required]
        [Display(Name = "Rola")]
        public int Roleid { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Data stworzenia jest wymagana")]
        [Display(Name="Data stworzenia")]
        public DateTime Creationdate { get; set; }
        [Display(Name = "Rola")]
        public virtual Role Role { get; set; }
        public virtual Credentials Credentials { get; set; }
    }
}
