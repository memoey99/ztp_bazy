﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ZTP_BAZY.Models
{
    public partial class Shop
    {
        public Shop()
        {
            Order = new HashSet<Order>();
        }

        public int Shopid { get; set; }
        [Display(Name = "Nazwa sklepu")]
        [Required(ErrorMessage = "Nazwa jest wymagana")]
        [MaxLength(50,ErrorMessage ="Za długa nazwa")]
        public string Shopname { get; set; }
        [Display(Name = "Ulica")]
        [Required(ErrorMessage = "Ulica jest wymagana")]
        [MaxLength(40, ErrorMessage = "Za długa nazwa ulicy")]
        public string Street { get; set; }
        [Display(Name = "Numer")]
        [Required(ErrorMessage = "Numer jest wymagany")]
        public int Number { get; set; }
        [Display(Name = "Kod pocztowy")]
        [MaxLength(6, ErrorMessage = "Kod niepoprawny")]
        [DataType(DataType.PostalCode)]
        public string Zipcode { get; set; }
        [Display(Name = "Miejscowość")]
        [Required(ErrorMessage = "Miejscowość jest wymagana")]
        [MaxLength(30, ErrorMessage = "Za długa nazwa")]
        public string City { get; set; }
        [Display(Name = "Numer telefonu")]
        [DataType(DataType.PhoneNumber,ErrorMessage ="Proszę podać poprawny numer")]
        [MaxLength(12,ErrorMessage ="numer telefonu za długi")]
        public string Phonenumber { get; set; }
        [Display(Name = "Adres e-mail")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Prosze podać poprawny adres e-mail")]
        [MaxLength(30, ErrorMessage = "Za długi aders, max 30 znaków")]
        public string Mailaddress { get; set; }
        [Display(Name = "Adres URL")]
        [RegularExpression(@"^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$",ErrorMessage ="Niepoprawny adres")]
        [MaxLength(30, ErrorMessage = "Za długi aders, max 30 znaków")]
        public string Url { get; set; }

        public virtual ICollection<Order> Order { get; set; }
    }
}
